import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useEffect, useState } from 'react';

export default function App() {
  const [count, setCount] = useState(0);
  const[dado, setDado] = useEffect(0);
  function rodarDado(){
    setDado(Math.floor(Math.random() * 6) + 1); //FLOOR - é uma função de aproximação. RANDOM - sorteia um númeo de 0 a 10, decimal.
  }
  return (
    <View style={styles.container}>
      <ScrollView>
      {/* <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text>
      <Text>Hello World!!!</Text> */}
      <Text> Clique para contar: {count} </Text>
      <TouchableOpacity
        style = {styles.teste}
        onPress={() => setCount(count + 1)}
      >
      <Text>Clique!</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.teste}
        onPress={() => rodarDado()}
      >
        <Text>Jogue o dado!</Text>

      </TouchableOpacity>
      <Text>Número Sorteado: {dado}</Text>
      </ScrollView>
      {/* <StatusBar style="auto" /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c2dbf0',
    alignItems: 'center',
    justifyContent: 'center',
  },

  teste: {
    color: "black",
    backgroundColor: "pink",
    width: 90,
    borderRadius: 25,
    alignItems: "center"
  },
});

//Open up App.js to start working on your app!